mod config {
    use std::{
        collections::{HashMap, HashSet},
        fs::{self, File},
        io::{self, BufRead, BufReader},
        path::PathBuf,
    };

    static JSON_ROOT: &str = "json";
    static REQUESTS_DIR: &str = "requests";
    static RESPONSES_DIR: &str = "responses";

    #[derive(Debug)]
    pub struct MessageSpec {
        pub name: String,
        pub request: String,
        pub response: String,
    }

    impl MessageSpec {
        fn new(name: String, request: String, response: String) -> Self {
            Self {
                name,
                request,
                response,
            }
        }
    }

    pub fn read_path(path: PathBuf) -> io::Result<String> {
        let handle = File::open(path)?;

        let lines: Result<Vec<String>, io::Error> = BufReader::new(handle).lines().collect();

        lines.map(|lines| lines.join(" "))
    }

    pub fn parse_spec_name(path: &PathBuf) -> String {
        path.file_stem()
            .expect("Got an invalid file name in the JSON dir")
            .to_string_lossy()
            .into()
    }

    pub fn load_json(path: PathBuf) -> io::Result<HashMap<String, String>> {
        let json_dir = path.as_path();

        let listing = match fs::read_dir(json_dir) {
            Ok(l) => l,
            // The io::Error type doesn't carry information about what wasn't
            // found. There are all manner of crates for dealing with this, or I
            // could make my own error type. For now, we'll just log a little
            // extra here.
            Err(ioe) => {
                println!("No such directory: {}", json_dir.display());
                return Err(ioe);
            }
        };

        let mut result = HashMap::new();

        for maybe_entry in listing {
            let entry = maybe_entry?;
            let file_path = entry.path();
            let file_name = parse_spec_name(&file_path);

            let contents = read_path(file_path.clone())?;

            result.insert(file_name, contents);
        }

        Ok(result)
    }

    pub fn load_message_specs() -> io::Result<HashMap<String, MessageSpec>> {
        let requests_path: PathBuf = ["/", JSON_ROOT, REQUESTS_DIR].iter().collect();
        let responses_path: PathBuf = ["/", JSON_ROOT, RESPONSES_DIR].iter().collect();

        let requests = load_json(requests_path)?;
        let responses = load_json(responses_path)?;

        let request_keys: HashSet<String> = requests.keys().cloned().collect();
        let response_keys: HashSet<String> = responses.keys().cloned().collect();

        // TODO[gastove|2020-09-07] This should be a compare and diff, so that we
        // can actually log what's wrong/missing/different, instead of a
        // straight assert.
        if request_keys != response_keys {
            let req_has: Vec<String> = request_keys.difference(&response_keys).cloned().collect();
            let resp_has: Vec<String> = response_keys.difference(&request_keys).cloned().collect();

            if req_has.len() > 0 || resp_has.len() > 0 {
                if req_has.len() > 0 {
                    println!(
                        "Requests has {} entries not in responses: {:#?}",
                        req_has.len(),
                        req_has
                    );
                }

                if resp_has.len() > 0 {
                    println!(
                        "Responses has {} entries not in requests: {:#?}",
                        resp_has.len(),
                        resp_has
                    );
                }

                println!("\n==> Message Spec(s) missing, correct and rety <==\n");
                panic!();
            }
        }

        let loaded = request_keys
            .iter()
            .fold(HashMap::new(), |mut state, spec_name| {
                let req = requests
                    .get(spec_name)
                    .expect("Failed to load val from requests map")
                    .clone();
                let resp = responses
                    .get(spec_name)
                    .expect("Failed to load val from responses map")
                    .clone();

                let msg_spec = MessageSpec::new(spec_name.clone(), req, resp);

                state.insert(spec_name.clone(), msg_spec);

                state
            });

        Ok(loaded)
    }
}

use draw_bridge::api::messages::{Request, Response};
use serde_json::{from_str, to_string};
use std::fmt::Debug;

fn panic_on_error<T, E, R>(
    res: Result<T, E>,
    action: &str,
    kind: &str,
    spec_name: &String,
    json: R,
) -> T
where
    E: 'static + Send + Sync + Debug,
    R: std::fmt::Display,
{
    match res {
        Ok(r) => r,
        Err(e) => {
            println!(
                "Failed to {} the {} {}; json was invalid:\n\t{}",
                action, spec_name, kind, json
            );
            panic!("Serde error was:\n{:?}", e);
        }
    }
}

#[test]
#[ignore]
fn test_request_spec_round_tripping() {
    let specs = config::load_message_specs().expect("Failed to load test specs");

    for (spec_name, spec) in specs {
        // round_trip_and_assert::<Request>(&spec_name, &spec.request);
        let loaded_first: Request = from_str(&spec.request).expect("Failed to deserialize JSON");

        let intermediate: String = to_string(&loaded_first).expect("Failed to serialize to JSON");

        let loaded_second: Request = from_str(&intermediate).expect("Failed to deserialize JSON");

        assert_eq!(
            loaded_first, loaded_second,
            "Could not round-trip request for {}",
            spec_name
        );
    }
}

#[test]
#[ignore]
fn test_response_spec_round_tripping() {
    let specs = config::load_message_specs().expect("Failed to load test specs");

    for (spec_name, spec) in specs {
        let loaded_first: Response = panic_on_error(
            from_str(&spec.response),
            "deserialize",
            "response",
            &spec_name,
            &spec.response,
        );

        let intermediate: String = panic_on_error(
            to_string(&loaded_first),
            "serialize",
            "response",
            &spec_name,
            &loaded_first,
        );

        let loaded_second: Response = panic_on_error(
            from_str(&intermediate),
            "deserialize",
            "response",
            &spec_name,
            &intermediate,
        );

        assert_eq!(
            loaded_first, loaded_second,
            "Could not round-trip request for {}",
            spec_name
        );
    }
}

// #[test]
// #[ignore]
// fn test_all_spec_pairs_present() {
//     assert_eq!(1, 2);
// }
