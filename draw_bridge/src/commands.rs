use std::time::Duration;

use {
    crate::{
        api::messages::{QueryableAttributes, SettableAttributes},
        configuration::{GettableConfig, SettableConfigs},
        instructions::EBBResponse,
        plotter::{Device, RegisteredPlotters},
    },
    im::HashMap,
    log::{debug, info},
    regex::Regex,
    std::fmt,
};

lazy_static! {
    static ref VERSION_RE: Regex =
        Regex::new(r"^EBBv13_and_above EB Firmware Version (\d+)\.(\d+)(?:\.(\d+))?$").unwrap();
}

#[derive(Clone, Debug)]
pub enum Error {
    InvalidCommand,
    /// In the rare and undocumented case when the plotter itself returns an
    /// error, this is what we get to work with.
    PlotterError(String),
    IoError(String),
}

impl fmt::Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let msg = match self {
            Self::InvalidCommand => String::from("Invalid command"),
            Self::PlotterError(s) => format!("Parsing plotter response failed with: {}", s),
            Self::IoError(s) => format!("IO Error while executing command: {}", s),
        };
        write!(f, "{}", msg)
    }
}

impl From<std::io::Error> for Error {
    fn from(ioe: std::io::Error) -> Self {
        Error::IoError(ioe.to_string())
    }
}

impl From<String> for Error {
    fn from(s: String) -> Self {
        Error::PlotterError(s)
    }
}

pub type Result = std::result::Result<EBBResponse, Error>;

// An internal place to manage the functions that actually _run_ all this stuff.
mod impls {

    use {
        super::{Delta, Error, Point, Result},
        crate::{
            instructions::{EBBInstruction, EBBResponse, PenPosition},
            plotter::Device,
        },
        std::time::Duration,
    };

    fn simple_instruction(inst: EBBInstruction, dev: &mut Box<dyn Device>) -> Result {
        simple_instruction_with_duration(inst, dev, Duration::from_millis(5))
    }

    fn simple_instruction_with_duration(
        inst: EBBInstruction,
        dev: &mut Box<dyn Device>,
        duration: Duration,
    ) -> Result {
        let bytes = inst.as_bytes();

        let resp = dev
            .execute(&bytes, duration, inst.responses())
            .map(|res| inst.parse_response(res))??;

        Ok(resp)
    }

    pub fn toggle_pen(mut dev: Box<dyn Device>) -> Result {
        let instruction = EBBInstruction::TogglePen;

        simple_instruction(instruction, &mut dev)
    }

    pub fn raise_pen(mut dev: Box<dyn Device>) -> Result {
        let instruction = EBBInstruction::SetPen(PenPosition::Up);

        simple_instruction(instruction, &mut dev)
    }

    pub fn lower_pen(mut dev: Box<dyn Device>) -> Result {
        let instruction = EBBInstruction::SetPen(PenPosition::Down);

        simple_instruction(instruction, &mut dev)
    }

    // duration as a u32 option of millis
    pub fn go_to(
        deltas: Vec<Delta>,
        maybe_duration: Option<Duration>,
        mut dev: Box<dyn Device>,
    ) -> Result {
        let mut results: Vec<EBBResponse> = Vec::new();
        let duration = maybe_duration.unwrap_or(Duration::from_millis(2000));
        for delta in deltas {
            let instruction = EBBInstruction::MixedGeometryMove {
                x: delta.x_distance,
                y: delta.y_distance,
                duration,
            };

            let result = simple_instruction_with_duration(instruction, &mut dev, duration)?;
            results.push(result);
        }

        Ok(EBBResponse::Multiple(results))
    }

    pub fn version(dev: &mut Box<dyn Device>) -> Result {
        let instruction = EBBInstruction::Version;

        simple_instruction(instruction, dev)
    }

    pub fn pause(mut dev: Box<dyn Device>, duration: Duration) -> Result {
        let instruction = EBBInstruction::MixedGeometryMove {
            x: 0,
            y: 0,
            duration,
        };

        simple_instruction_with_duration(instruction, &mut dev, duration)
    }

    pub fn home(mut dev: Box<dyn Device>) -> Result {
        let mut results: Vec<EBBResponse> = Vec::new();

        let pen_up = EBBInstruction::SetPen(PenPosition::Up);
        let home = EBBInstruction::HomeMove;

        let pen_up_res = simple_instruction(pen_up, &mut dev)?;
        results.push(pen_up_res);

        let home_res = simple_instruction(home, &mut dev)?;
        results.push(home_res);

        Ok(EBBResponse::Multiple(results))
    }

    pub fn get_nickname(dev: &mut Box<dyn Device>) -> Result {
        let instruction = EBBInstruction::QueryNickname;

        simple_instruction(instruction, dev)
    }

    pub fn set_nickname(nickname: String, mut dev: Box<dyn Device>) -> Result {
        let instruction = EBBInstruction::SetNickname(nickname);

        simple_instruction(instruction, &mut dev)
    }

    pub fn query_motors(dev: &mut Box<dyn Device>) -> Result {
        let instruction = EBBInstruction::QueryMotorStatus;

        simple_instruction(instruction, dev)
    }

    pub fn query_current_pen_state(mut dev: Box<dyn Device>) -> Result {
        let pen_position_instr = EBBInstruction::QueryPen;

        let pen_position_result = simple_instruction(pen_position_instr, &mut dev)?;

        let x_y_position_instr = EBBInstruction::QueryCurrentPenPosition;

        let x_y_pos = simple_instruction(x_y_position_instr, &mut dev)?;

        match (pen_position_result.clone(), x_y_pos.clone()) {
            (EBBResponse::PenPosition(p), EBBResponse::Steps { x_pos, y_pos }) => {
                Ok(EBBResponse::PenState {
                    position: p,
                    x: x_pos,
                    y: y_pos,
                })
            }
            (_, _) => Err(Error::PlotterError(
                "I honestly don't know what happened".into(),
            )),
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Point {
    x: i32,
    y: i32,
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
pub struct Delta {
    x_distance: i32,
    y_distance: i32,
}

impl Delta {
    pub fn new(x_distance: i32, y_distance: i32) -> Self {
        Delta {
            x_distance,
            y_distance,
        }
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum Command {
    TogglePen {
        plotter: RegisteredPlotters,
    },
    RaisePen {
        plotter: RegisteredPlotters,
    },
    LowerPen {
        plotter: RegisteredPlotters,
    },
    MovePen {
        plotter: RegisteredPlotters,
        deltas: Vec<Delta>,
        duration: Option<Duration>,
    },
    Home {
        plotter: RegisteredPlotters,
    },
    QueryPlotterAttributes {
        plotter: RegisteredPlotters,
        attributes: Vec<QueryableAttributes>,
    },
    SetPlotterAttributes {
        plotter: RegisteredPlotters,
        attributes: Vec<SettableAttributes>,
    },
    Pause {
        plotter: RegisteredPlotters,
        duration: Duration,
    },
}

impl Command {
    pub fn run(&self, mut dev: Box<dyn Device>) -> Result {
        debug!("Running Command {:#?}", self);
        match self {
            Command::TogglePen { .. } => impls::toggle_pen(dev),
            Command::RaisePen { .. } => impls::raise_pen(dev),
            Command::LowerPen { .. } => impls::lower_pen(dev),
            Command::MovePen {
                deltas, duration, ..
            } => impls::go_to(deltas.clone(), *duration, dev),
            Command::Home { .. } => impls::home(dev),
            Command::QueryPlotterAttributes {
                plotter,
                attributes,
            } => {
                let got: Vec<EBBResponse> = attributes
                    .iter()
                    .flat_map(|attr| -> Result {
                        match attr {
                            QueryableAttributes::FirmwareVersion => impls::version(&mut dev),
                            QueryableAttributes::Nickname => impls::get_nickname(&mut dev),
                            QueryableAttributes::MotorStatus => impls::query_motors(&mut dev),
                            QueryableAttributes::PenState => unimplemented!(),
                        }
                    })
                    .collect();

                Ok(EBBResponse::Multiple(got))
            }
            Command::SetPlotterAttributes {
                plotter,
                attributes,
            } => unimplemented!(),
            Command::Pause { duration, .. } => impls::pause(dev, *duration),
        }
    }
}
