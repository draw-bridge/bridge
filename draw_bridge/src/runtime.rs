use std::path::PathBuf;

use log::debug;

use crate::udev;

pub struct Config {}

pub struct Context {
    pub config: Config,
    pub socket: Option<PathBuf>,
}

impl Context {
    pub fn new() -> Self {
        let cfg = Config {};

        match udev::scan_for_plotter() {
            Ok(pathbuf) => {
                debug!("Found plotter at {}", pathbuf.as_path().to_str().unwrap());
                Self {
                    config: cfg,
                    socket: Some(pathbuf),
                }
            }
            Err(_) => Self {
                config: cfg,
                socket: None,
            },
        }
    }
}
