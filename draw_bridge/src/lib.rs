#![warn(clippy::all)]
#![allow(unused)]

//! # draw bridge
//!
//! `draw bridge` is a software driver for controlling an EiBotBoard drawing robot
//! from other, higher-level software. It aims to expose a set of APIs into in EBB
//! device via a JSON RPC-style IPC socket.
//!
//! **Current Project Status** `WIP.MAX_WIP`. Seriously, this... does work, in a
//! limited, literal sense, but is under a lot of development and testing.
//!
//! ### Questions
//!
//! - Q: _So how will I know it's ready for common use?_
//!   A: Watch for the 1.0 release.

extern crate libudev;

#[macro_use]
extern crate lazy_static;
extern crate regex;

#[macro_use]
extern crate serde_derive;
extern crate serde;
extern crate serde_json;

extern crate serial_core;
extern crate serial_unix;

extern crate env_logger;
extern crate log;
extern crate simplelog;

//----------------------------------- local -----------------------------------//
// Note, anything we want to use in e.g. an integration test needs to be marked public.
// ...which means it's mostly public.
// ...which feels really weird. Might... wanna re-consider this one of these days.
pub mod api;
pub mod commands;
pub mod configuration;
pub mod instructions;
pub mod plotter;
mod runtime;
pub mod socket;
mod udev;
