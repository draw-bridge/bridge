use std::path::PathBuf;

mod property_names {
    pub static VENDOR_NAME: &str = "ID_VENDOR";
    pub static VENDOR_ID: &str = "ID_VENDOR_ID";
    #[allow(unused)]
    pub static PRODUCT_ID: &str = "ID_MODEL_ID";
}

mod plotter_configs {
    pub static VENDOR_ID: &str = "04d8";
    pub static VENDOR: &str = "SchmalzHaus";
    #[allow(unused)]
    pub static MODEL: &str = "EiBotBoard";
}

/// Search connected devices in userspace (using libudev), trying to find a
/// known EiBotBoard plotter. If one is found, return it -- otherwise, let us
/// know what went wrong.
pub fn scan_for_plotter() -> Result<PathBuf, &'static str> {
    // Create our udev context and an enumerator to scan devices with
    let context = libudev::Context::new().unwrap();
    let mut enumerator = libudev::Enumerator::new(&context).unwrap();

    // Configuring the enumerator to filter for TTY EiBotBoard devices
    enumerator.match_subsystem("tty").unwrap();
    enumerator
        .match_property(&property_names::VENDOR_ID, plotter_configs::VENDOR_ID)
        .unwrap();
    enumerator
        .match_property(&property_names::VENDOR_NAME, plotter_configs::VENDOR)
        .unwrap();

    let devices: Vec<libudev::Device> = enumerator.scan_devices().unwrap().collect();

    match devices.len() {
        0 => Err("Failed to find any plotters; is your plotter plugged in?"),
        1 => {
            let device = &devices[0];

            match device.devnode() {
                Some(socket) => Ok(PathBuf::from(socket)),
                None => Err("Failed to find a socket"),
            }
        }
        _ => Err("Found a surprisingly large number of plotters! Uh oh."),
    }
}

#[allow(unused)]
pub fn describe() {
    // Create our udev context and an enumerator to scan devices with
    let context = libudev::Context::new().unwrap();
    let mut enumerator = libudev::Enumerator::new(&context).unwrap();

    // Configuring the enumerator to filter for TTY EiBotBoard devices
    enumerator.match_subsystem("tty").unwrap();
    enumerator
        .match_property(&property_names::VENDOR_ID, plotter_configs::VENDOR_ID)
        .unwrap();
    enumerator
        .match_property(&property_names::VENDOR_NAME, plotter_configs::VENDOR)
        .unwrap();

    let devices: Vec<libudev::Device> = enumerator.scan_devices().unwrap().collect();

    let device = &devices[0];

    println!("Plotter has properties:");
    for property in device.properties() {
        println!("\t- {:?} = {:?}", property.name(), property.value());
    }

    println!("Plotter has attributes:");
    for attribute in device.attributes() {
        println!("\t- {:?} = {:?}", attribute.name(), attribute.value());
    }
}
