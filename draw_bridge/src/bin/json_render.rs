#[macro_use]
extern crate clap;
extern crate colored_json;
extern crate draw_bridge;
extern crate serde_json;

use {
    clap::{App, Arg},
    colored_json::prelude::*,
    draw_bridge::{
        api::messages::{
            ApiError, ApiResult, Method, MethodParams, QueryableAttributes, Request, Response,
            SettableAttributes,
        },
        commands::Delta,
        configuration::{GettableConfig, SettableConfigs},
        instructions::{EBBResponse, PenPosition},
        plotter::RegisteredPlotters,
    },
    std::{convert::TryFrom, time::Duration},
};

fn params_for_method(method: &Method) -> MethodParams {
    let plotter = RegisteredPlotters::AxiDrawV3A3;
    let plotter_only = MethodParams::PlotterOnly { plotter };

    match method {
        Method::GetConfig => MethodParams::GetConfigParams {
            plotter,
            configs: vec![GettableConfig::XTravel, GettableConfig::YTravel],
        },
        Method::SetConfig => MethodParams::SetConfigParams {
            plotter,
            configs: vec![
                SettableConfigs::XTravel(1000.0),
                SettableConfigs::YTravel(1000.0),
            ],
        },
        Method::QueryPlotterAttribute => MethodParams::QueryPlotterAttributeParams {
            plotter,
            attributes: vec![QueryableAttributes::FirmwareVersion],
        },
        Method::SetPlotterAttribute => MethodParams::SetPlotterAttributeParams {
            plotter,
            attributes: vec![SettableAttributes::Nickname("desmond".to_string())],
        },
        Method::PlotterRaisePen
        | Method::Home
        | Method::PlotterLowerPen
        | Method::PlotterTogglePen => plotter_only,

        Method::PlotterMovePen => MethodParams::MovePen {
            plotter,
            deltas: vec![Delta::new(25, 25), Delta::new(50, 50)],
            duration: Some(1000),
        },

        Method::Pause => MethodParams::PauseParams {
            plotter,
            duration: 1,
        },
    }
}

fn construct_request(method: Method) -> Request {
    let params = params_for_method(&method);

    Request::new(method, params, Some(0))
}

fn result_for_method(method: &Method) -> ApiResult {
    match method {
        Method::GetConfig => {
            let cfg = draw_bridge::configuration::load_config_for_plotter(
                &RegisteredPlotters::AxiDrawV3A3,
            );

            ApiResult::Config(cfg)
        }
        Method::QueryPlotterAttribute => {
            let fwv = EBBResponse::Version {
                major: 2,
                minor: 6,
                patch: 0,
            };

            let pen_state = EBBResponse::PenState {
                position: PenPosition::Up,
                x: 100,
                y: 100,
            };

            let nickname = EBBResponse::Nickname {
                nickname: "Susan".into(),
            };

            ApiResult::Results(vec![fwv, pen_state, nickname])
        }
        Method::SetPlotterAttribute => unimplemented!(),
        Method::PlotterLowerPen
        | Method::SetConfig
        | Method::PlotterTogglePen
        | Method::PlotterRaisePen
        | Method::Pause
        | Method::Home => EBBResponse::Unit.into(),
        Method::PlotterMovePen => {
            vec![EBBResponse::Unit, EBBResponse::Unit, EBBResponse::Unit].into()
        }
    }
}

fn construct_response(method: Method) -> Response {
    let result = result_for_method(&method);

    Response::new(Some(result), None)
}

fn main() {
    let app = App::new("jsonrender")
        .author("Ross Donaldson")
        .about("A utility for rendering DrawBridge API messages as JSON")
        .version(&crate_version!()[..])
        .arg(
            Arg::with_name("method")
                .long("method")
                .short("m")
                .required(true)
                .takes_value(true),
        )
        .get_matches();
    let maybe_method = app.value_of("method").unwrap();

    match Method::try_from(maybe_method) {
        Ok(method) => {
            let request = construct_request(method);
            let response = construct_response(method);

            let request_json = serde_json::to_string_pretty(&request)
                .expect("Failed to render request JSON")
                .to_colored_json_auto()
                .expect("Failed to colorize");

            let response_json = serde_json::to_string_pretty(&response)
                .expect("Failed to render response JSON")
                .to_colored_json_auto()
                .expect("Failed to colorize");

            println!(
                "Method {}\nRequest:\n{}\nWhich produces JSON:\n{}\nAnd Response:\n{}\nWith JSON:\n{}",
                method, request, request_json, response, response_json,
            );
        }
        Err(e) => panic!("Alas: {}", e),
    }
}
