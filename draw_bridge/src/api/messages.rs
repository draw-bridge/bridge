//! # api::messages
//!
//! The `messages` module defines all the message types that can be passed back
//! and forth via the API -- client requests and DrawBridge responses.
//!
//! DrawBridge defines its API using [JSON-RPC](https://www.jsonrpc.org/). We
//! aim to be as compliant as possible, though as with all things, there will be
//! places in which our implementation could be better.

use {
    crate::{
        commands::{Command, Delta, Error as CmdError},
        configuration::{ConfigurationError, GettableConfig, PlotterConfig, SettableConfigs},
        instructions::EBBResponse,
        plotter::RegisteredPlotters,
    },
    im::HashMap,
    serde_json::Error as SerdeError,
    std::{
        convert::{From, TryFrom, TryInto},
        fmt,
        time::Duration,
    },
};

// TODO[gastove|2020-06-21] There are very good odds this total error list can
// be substantially cut down.

/// The ApiError type models all the errors we admit, over the socket, back to a
/// client program. Serde errors, pen plotter execution errors, invalid command
/// errors: the whole kit, and also the kaboodle. The enchilada entire.
///
/// Accordingly, ApiError also implements `From` on all the other error types we
/// have to work with.
#[derive(Debug)]
pub enum ApiError {
    TypeKeyMissing,
    ParamsMissing,
    ParamMissing(String),
    ParamInvalid(String),
    #[allow(unused)]
    APINotImplemented(String),
    #[allow(unused)]
    CommandNotImplemented(String),
    #[allow(unused)]
    UnknownMessageType(String),
    IoError(String),
    PlotterError(String),
    SerdeError(SerdeError),
}

impl ApiError {
    /// JSON-RPC reserves everything from -32768 -> -32000 for Official Use. I'm
    /// going to start my own error codes at 100.
    pub fn error_code(&self) -> i16 {
        match self {
            ApiError::TypeKeyMissing => 100,
            ApiError::ParamsMissing | ApiError::ParamMissing(_) => -32602,
            ApiError::ParamInvalid(_) => -32602,
            ApiError::APINotImplemented(_) => 105,
            ApiError::CommandNotImplemented(_) => 106,
            ApiError::UnknownMessageType(_) => 107,
            ApiError::IoError(_) => 108,
            ApiError::PlotterError(_) => 109,
            ApiError::SerdeError(sde) => {
                match sde.classify() {
                    // TODO[gastove|2020-06-21] Technically, we should return
                    // -32601 if the method is invalid. However: serde is going
                    // to roll that right in to its `Data` error. Eventually, we
                    // should introspect the error further and return an error
                    // code of greater precision.
                    serde_json::error::Category::Data => -32600,
                    // For lack of a more compelling plan, everything else will
                    // be a "parse error".
                    _ => -32700,
                }
            }
        }
    }
}

impl fmt::Display for ApiError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match self {
            Self::APINotImplemented(api) => format!("API named '{}' is not implemented", api),
            Self::ParamsMissing => format!("No `params` key sent in the request"),
            Self::ParamMissing(s) => format!("Expected a param named {}, but none was present", s),
            Self::ParamInvalid(s) => format!("Parameter found to be invalid: {}", s),
            Self::CommandNotImplemented(cmd) => format!("Command '{}' is not implemented", cmd),
            Self::TypeKeyMissing => String::from("Key 'type' was missing from the command map"),
            Self::UnknownMessageType(msg) => {
                format!("Message could not be parsed to a known type: {}", msg)
            }
            Self::IoError(s) => format!(
                "IO error encountered while communicating with the plotter: {}",
                s
            ),
            Self::PlotterError(s) => format!("Plotter reported an error: {}", s),
            Self::SerdeError(sde) => format!("Failed to deserialize a JSON object: {}", sde),
        };
        write!(f, "Failed to parse message: {}", s)
    }
}

impl From<SerdeError> for ApiError {
    fn from(se: SerdeError) -> Self {
        ApiError::SerdeError(se)
    }
}

impl From<CmdError> for ApiError {
    fn from(ce: CmdError) -> Self {
        match ce {
            // TODO[gastove|2020-06-20] Need to pass invalid command names
            // around, and also, we need to distinguish between invalid and
            // unimplemented.
            CmdError::InvalidCommand => Self::CommandNotImplemented("tbd".into()),
            CmdError::PlotterError(s) => Self::PlotterError(s),
            CmdError::IoError(s) => Self::IoError(s),
        }
    }
}

impl From<ConfigurationError> for ApiError {
    fn from(cfge: ConfigurationError) -> Self {
        match cfge {
            ConfigurationError::InvalidValue(i) | ConfigurationError::InvalidParams(i) => {
                Self::ParamInvalid(i)
            }
        }
    }
}

/// A message sent from the user to the API.
#[derive(Copy, Debug, Serialize, Deserialize, Clone, PartialEq)]
pub enum Method {
    /// Qeury DrawBridge configuration
    #[serde(rename = "v1.config.get")]
    GetConfig,
    /// Set DrawBridge configuration
    #[serde(rename = "v1.config.set")]
    SetConfig,
    /// Query the plotter for its current state.
    #[serde(rename = "v1.core.query_plotter_attrs")]
    QueryPlotterAttribute,
    /// Set certain settable attributes (e.g. nickname) on the plotter.
    #[serde(rename = "v1.core.set_plotter_attrs")]
    SetPlotterAttribute,
    /// Raises the Plotter's Pen
    #[serde(rename = "v1.core.raise_pen")]
    PlotterRaisePen,
    /// Lowers the Plotter's pen
    #[serde(rename = "v1.core.lower_pen")]
    PlotterLowerPen,
    /// Toggle the pens position
    #[serde(rename = "v1.core.toggle_pen")]
    PlotterTogglePen,
    /// Move the pen by the given number of steps on the X and Y axes
    #[serde(rename = "v1.core.move_pen")]
    PlotterMovePen,
    /// Move the pen to the origin
    #[serde(rename = "v1.core.home")]
    Home,
    /// Pause motion for a given number of milliseconds
    #[serde(rename = "v1.core.pause")]
    Pause,
}

impl fmt::Display for Method {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let term = match self {
            Method::GetConfig => "v1.config.get",
            Method::SetConfig => "v1.config.set",
            Method::PlotterRaisePen => "v1.core.raise_pen",
            Method::PlotterLowerPen => "v1.core.lower_pen",
            Method::PlotterTogglePen => "v1.core.toggle_pen",
            Method::PlotterMovePen => "v1.core.move_pen",
            Method::Home => "v1.core.home",
            Method::QueryPlotterAttribute => "v1.core.query_plotter_attr",
            Method::SetPlotterAttribute => "v1.core.set_plotter_attr",
            Method::Pause => "v1.core.pause",
        };
        write!(f, "{}", term)
    }
}

impl TryFrom<&str> for Method {
    type Error = String;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "v1.config.get" => Ok(Method::GetConfig),
            "v1.config.set" => Ok(Method::SetConfig),
            "v1.core.raise_pen" => Ok(Method::PlotterRaisePen),
            "v1.core.lower_pen" => Ok(Method::PlotterLowerPen),
            "v1.core.toggle_pen" => Ok(Method::PlotterTogglePen),
            "v1.core.move_pen" => Ok(Method::PlotterMovePen),
            "v1.core.home" => Ok(Method::Home),
            "v1.core.query_plotter_attr" => Ok(Method::QueryPlotterAttribute),
            "v1.core.set_plotter_attr" => Ok(Method::SetPlotterAttribute),
            "v1.core.pause" => Ok(Method::Pause),
            _ => Err(format!("Cannot find a Method that matches {}", value)),
        }
    }
}

impl TryFrom<String> for Method {
    type Error = String;

    fn try_from(value: String) -> Result<Self, Self::Error> {
        value.as_str().try_into()
    }
}

#[derive(Debug, Clone, Eq, PartialEq, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum QueryableAttributes {
    FirmwareVersion,
    Nickname,
    MotorStatus,
    PenState,
}

#[derive(Debug, Clone, Eq, Hash, PartialEq, Serialize, Deserialize)]
pub enum SettableAttributes {
    Nickname(String),
}

#[derive(Debug, Clone, PartialEq, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum MethodParams {
    #[serde(rename = "v1.core.plotter_only")]
    PlotterOnly { plotter: RegisteredPlotters },
    #[serde(rename = "v1.core.move_pen_params")]
    MovePen {
        plotter: RegisteredPlotters,
        deltas: Vec<Delta>,
        duration: Option<u64>,
    },
    #[serde(rename = "v1.config.get_params")]
    GetConfigParams {
        plotter: RegisteredPlotters,
        configs: Vec<GettableConfig>,
    },
    #[serde(rename = "v1.config.set_params")]
    SetConfigParams {
        plotter: RegisteredPlotters,
        configs: Vec<SettableConfigs>,
    },
    #[serde(rename = "v1.core.query_plotter_attrs_params")]
    QueryPlotterAttributeParams {
        plotter: RegisteredPlotters,
        attributes: Vec<QueryableAttributes>,
    },
    #[serde(rename = "v1.core.set_plotter_attrs_params")]
    SetPlotterAttributeParams {
        plotter: RegisteredPlotters,
        attributes: Vec<SettableAttributes>,
    },
    #[serde(rename = "v1.core.pause_params")]
    PauseParams {
        plotter: RegisteredPlotters,
        duration: u64,
    },
}

impl fmt::Display for MethodParams {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let term = match self {
            MethodParams::PlotterOnly { plotter } => "v1.core.plotter_only",
            MethodParams::MovePen {
                plotter,
                deltas,
                duration,
            } => "v1.core.move_pen_params",
            MethodParams::GetConfigParams { .. } => "v1.core.get_config_params",
            MethodParams::SetConfigParams { .. } => "v1.core.set_pen_params",
            MethodParams::QueryPlotterAttributeParams {
                plotter,
                attributes,
            } => "v1.core.query_plotter_attrs_params",
            MethodParams::SetPlotterAttributeParams {
                plotter,
                attributes,
            } => "v1.core.set_plotter_attrs_params",
            MethodParams::PauseParams { plotter, duration } => "v1.core.pause_params",
        };

        write!(f, "{}", term)
    }
}

impl MethodParams {
    pub fn get_plotter_ref(&self) -> &RegisteredPlotters {
        match self {
            MethodParams::PlotterOnly { plotter }
            | MethodParams::MovePen { plotter, .. }
            | MethodParams::GetConfigParams { plotter, .. }
            | MethodParams::SetConfigParams { plotter, .. }
            | MethodParams::QueryPlotterAttributeParams { plotter, .. }
            | MethodParams::PauseParams { plotter, .. }
            | MethodParams::SetPlotterAttributeParams { plotter, .. } => plotter,
        }
    }
}

// TODO[gastove|2021-01-04] All plotter durations are expressed in milliseconds,
// but have a maximum of something over 16 million -- making them an unsigned
// *24 bit* integer. This is... stupid.
//
// The good news is, it's very unlikely users will risk this. 16 million
// milliseconds is about 4.4 hours. Maybe that'll be useful for someone with
// High Concept Ideas, but... oof. There is, however, nothing for it but to
// implement error reporting. We need param validation in general.
//
// To do: param validation.
#[derive(Debug, Serialize, Deserialize, Clone, PartialEq)]
pub struct Request {
    #[serde(rename = "jsonrpc")]
    json_rpc: String,
    pub method: Method,
    pub params: MethodParams,
    id: Option<u32>,
}

impl Request {
    pub fn new(method: Method, params: MethodParams, id: Option<u32>) -> Self {
        Request {
            json_rpc: "2.0".to_string(),
            method,
            params,
            id,
        }
    }

    /// Validates that the given params are correct for a given method, and are
    /// correct of themselves. Returns a validated, groomed thingy for
    /// drawbridge to execute.
    pub fn parse_command(&self) -> Result<Command, ApiError> {
        match (self.method, self.params.clone()) {
            (Method::PlotterRaisePen, MethodParams::PlotterOnly { plotter }) => {
                Ok(Command::RaisePen { plotter })
            }
            (Method::PlotterLowerPen, MethodParams::PlotterOnly { plotter }) => {
                Ok(Command::LowerPen { plotter })
            }
            (Method::PlotterTogglePen, MethodParams::PlotterOnly { plotter }) => {
                Ok(Command::TogglePen { plotter })
            }
            (
                Method::PlotterMovePen,
                MethodParams::MovePen {
                    plotter,
                    deltas,
                    duration,
                },
            ) => Ok(Command::MovePen {
                plotter,
                deltas,
                duration: duration.map(|m| Duration::from_millis(m)),
            }),
            (Method::Home, MethodParams::PlotterOnly { plotter }) => Ok(Command::Home { plotter }),
            (
                Method::QueryPlotterAttribute,
                MethodParams::QueryPlotterAttributeParams {
                    plotter,
                    attributes,
                },
            ) => Ok(Command::QueryPlotterAttributes {
                plotter,
                attributes,
            }),
            (
                Method::SetPlotterAttribute,
                MethodParams::SetPlotterAttributeParams {
                    plotter,
                    attributes,
                },
            ) => Ok(Command::SetPlotterAttributes {
                plotter,
                attributes,
            }),
            (Method::SetConfig, MethodParams::SetConfigParams { .. }) => {
                panic!(
                    "The SetConfig method should never be parsed and run; something's gone wrong."
                );
            }
            (Method::GetConfig, MethodParams::GetConfigParams { .. }) => {
                panic!(
                    "The GetConfig method should never be parsed and run; something's gone wrong."
                );
            }
            (Method::Pause, MethodParams::PauseParams { plotter, duration }) => {
                let parsed = Duration::from_millis(duration);
                Ok(Command::Pause {
                    plotter,
                    duration: parsed,
                })
            }
            (m, p) => Err(ApiError::ParamInvalid(format!(
                "Params {} do not match method {}",
                p, m
            ))),
        }
    }

    pub fn is_config(&self) -> bool {
        match self.method {
            Method::GetConfig { .. } | Method::SetConfig { .. } => true,
            _ => false,
        }
    }

    // TODO[gastove|2021-01-04] This isn't right. This gets us re-serialized
    // JSON structs kinda over and over, which means variable nested escaping,
    // which is just gross. But: Serialize can't be used as a trait object, so I
    // can't return a HashMap<GettableConfig, Box<dyn Serialize>> or similar.
    // Gonna have to figure this out.
    pub fn get_config(
        &self,
        config: PlotterConfig,
    ) -> Result<HashMap<GettableConfig, String>, ConfigurationError> {
        match (self.method, self.params.clone()) {
            (
                Method::GetConfig,
                MethodParams::GetConfigParams {
                    plotter,
                    configs: configs_to_get,
                },
            ) => {
                let result = configs_to_get
                    .iter()
                    .cloned()
                    .fold(HashMap::new(), |hm, key| {
                        let cfg = config.get_val(&key);
                        hm.update(key, cfg)
                    });
                Ok(result)
            }
            (p, m) => Err(ConfigurationError::InvalidParams(format!(
                "Params {} do not match method {}",
                p, m
            ))),
        }
    }

    // TODO[gastove|2021-01-03] Need to validate, before this point, that the
    // method and params match.
    pub fn update_config(
        &self,
        config: PlotterConfig,
    ) -> Result<PlotterConfig, ConfigurationError> {
        match (self.method, self.params.clone()) {
            (
                Method::SetConfig,
                MethodParams::SetConfigParams {
                    plotter,
                    configs: set_configs,
                },
            ) => {
                let new_config = set_configs
                    .iter()
                    .fold(config, |cfg, to_update| cfg.update(to_update));
                Ok(new_config)
            }
            (p, m) => Err(ConfigurationError::InvalidParams(format!(
                "Params {} do not match method {}",
                p, m
            ))),
        }
    }
}

impl fmt::Display for Request {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:#?}", self)
    }
}

/// To be JSON-RPC compliant, an Error needs to be a structured type, with an
/// error code, message, and optional data. We're definitely omitting data for
/// now, but the other two should be correct.
///
/// There's some fiddliness here, because some error codes are pre-described and
/// others reserved. Hopefully I can get this right.
#[derive(Debug, Serialize, Deserialize, PartialEq)]
pub struct JsonRpcError {
    #[serde(rename = "code")]
    error_code: i16,
    message: String,
}

impl From<ApiError> for JsonRpcError {
    fn from(apie: ApiError) -> Self {
        Self {
            error_code: apie.error_code(),
            message: apie.to_string(),
        }
    }
}

#[derive(Debug, Clone, Deserialize, Serialize, PartialEq)]
#[serde(tag = "type", content = "data", rename_all = "snake_case")]
pub enum ApiResult {
    Ok,
    Message(EBBResponse),
    Config(PlotterConfig),
    Results(Vec<EBBResponse>),
}

impl From<EBBResponse> for ApiResult {
    fn from(response: EBBResponse) -> Self {
        println!("Matching {:#?}", response);
        match response {
            EBBResponse::Unit => Self::Ok,
            EBBResponse::Version { .. }
            | EBBResponse::Nickname { .. }
            | EBBResponse::Steps { .. }
            | EBBResponse::MotorStatus { .. }
            | EBBResponse::PenState { .. }
            | EBBResponse::PenPosition(_) => Self::Message(response),
            EBBResponse::Multiple(m) => Self::Results(m),
        }
    }
}

impl From<Vec<EBBResponse>> for ApiResult {
    fn from(responses: Vec<EBBResponse>) -> Self {
        Self::Results(responses)
    }
}

/// A response sent back *to* the user *from* the API.
#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub struct Response {
    #[serde(rename = "jsonrpc")]
    pub json_rpc: String,
    pub id: u32,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub result: Option<ApiResult>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub error: Option<JsonRpcError>,
}

impl Response {
    pub fn new(result: Option<ApiResult>, error: Option<JsonRpcError>) -> Self {
        Response {
            json_rpc: "2.0".into(),
            id: 0,
            result,
            error,
        }
    }
}

impl fmt::Display for Response {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:#?}", self)
    }
}

impl From<std::result::Result<ApiResult, ApiError>> for Response {
    fn from(result: std::result::Result<ApiResult, ApiError>) -> Self {
        match result {
            Ok(r) => Response::new(Some(r), None),
            Err(e) => Response::new(None, Some(e.into())),
        }
    }
}
