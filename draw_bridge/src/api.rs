//! # api
//!
//! The `api` module defines the method handler and message handling logic at
//! the heart of the DrawBridge IPC API.

pub mod messages;

use {
    crate::{
        api::messages::{ApiError, ApiResult, Request, Response},
        configuration::{load_config_for_plotter, PlotterConfig},
        plotter::{Device, Plotter, ReferenceDevice},
        runtime::Context,
    },
    log::{debug, error, info},
    serde::{Deserialize, Serialize},
    serial_core::SerialDevice,
    serial_unix::TTYPort,
    std::{
        convert::{From, Into},
        io::{BufRead, BufReader, Read, Write},
    },
};

/// load_device figures out which `Device` the API should use in responses. If
/// `reference = true`, use the in-memory `ReferenceDevice`; otherwise, open a
/// TTY connection to a connected plotter, or return None if no plotter is
/// available.
fn load_device(ctx: Context, reference: bool) -> Option<Box<dyn Device>> {
    match (ctx.socket, reference) {
        (_, true) => Some(Box::new(ReferenceDevice::default())), // reference plotter,
        (Some(socket), false) => {
            let mut tty =
                TTYPort::open(&socket.as_path()).expect("Failed to open reader on TTY to plotter");

            let timeout = std::time::Duration::from_millis(50);
            tty.set_timeout(timeout)
                .expect("Failed to set timeout on TTY");
            let p = Plotter::new(tty);
            // The Box needs to own the device reference to ensure it lives the correct span.
            Some(Box::new(p))
        }
        (None, false) => None,
    }
}

// This function has to handle several different kinds of errors, all of which
// need to convert to an ApiError. Accordingly, we use a lot of ?, which calls
// From implicitly.
fn parse_and_run(
    buf: &[u8],
    config: &mut Option<PlotterConfig>,
    device: Box<dyn Device>,
) -> Result<ApiResult, ApiError> {
    let request = serde_json::from_slice::<Request>(&buf)?;

    debug!("Received message:\n{:#?}", request);

    let config = config.or_else(|| Some(load_config_for_plotter(request.params.get_plotter_ref())));

    let result = if request.is_config() {
        if request.method == messages::Method::GetConfig {
            // This gets us kinda horrible results. Until I sort out how to fix
            // it, we return the whole works.
            // let got = request.get_config(config.unwrap())?;

            ApiResult::Config(config.unwrap())
        } else {
            let new_config = request.update_config(config.unwrap())?;

            let config = Some(new_config);

            ApiResult::Ok
        }
    } else {
        let cmd = request.parse_command()?;

        cmd.run(device)?.into()
    };

    Ok(result)
}

/// Runs the API as a Unix Socket handler, reading messages, generating
/// responses, and writing them back out.
pub fn make_api_handler<S>(mut stream: S, reference: bool) -> impl FnMut() -> () + Sync
where
    S: Read + Write + Sync, // A trait description of the most important features of a Socket.
{
    // Right. This is going to be a little clunky; we can't load a config until
    // we know which plotter we're loading for, but we need the loaded config to
    // persist through loops unmodified. We use an Option, with None indicating
    // an uninitialized config. Good luck to us.

    let mut config: Option<PlotterConfig> = None;

    move || loop {
        let mut buf = Vec::new();
        let mut reader = BufReader::new(&mut stream);
        match reader.read_until(b'\r', &mut buf) {
            Ok(bytes_read) => {
                debug!("Read {} bytes", bytes_read);

                if bytes_read == 0 {
                    debug!("Reached EOF, shutting down the handler");
                    break;
                }

                let ctx = Context::new();

                let device = load_device(ctx, reference).expect("Failed to connect to a plotter!");

                let result = parse_and_run(&buf, &mut config, device);

                let response: Response = result.into();

                debug!("Returning response:\n{:#?}", response);

                serde_json::to_writer(&mut stream, &response)
                    .expect("Failed to write response to stream");
                stream.flush().expect("Failed to flush intermediate buffer");

                debug!("Response done, closing connection");
            }
            Err(ioe) => {
                error!("Failed to read from domain socket: {}", ioe);
                break;
            }
        }
    }
}

#[cfg(test)]
mod api_test {

    use super::*;
    use std::convert::TryInto;
    use std::io::Cursor;

    use k9::assert_equal;

    // Useful if things get weird. The output is ugly and performance is bad, so
    // I don't typically enable it by default.
    fn init() {
        let _ = env_logger::builder().is_test(true).try_init();
    }

    // We wont be doing a lot of tests like this, because comparing two JSON
    // strings is a damn sight more brittle than comparing two struct instances.
    // But, having one test, just for gut-checking, that we put in a string and
    // get back a sensible string, gives me a feeling of confidence, so.
    #[test]
    fn test_api_returns_sensible_string() {
        init();

        let json_body = r#"
{
  "jsonrpc": "2.0",
  "method": "v1.core.query_plotter_attrs",
  "params": {
    "type": "v1.core.query_plotter_attrs_params",
    "plotter": "axidrawv3a3",
    "attributes": ["firmware_version"]
  }
}
"#;

        let expeced_response = r#"{"jsonrpc":"2.0","id":0,"result":{"type":"results","data":[{"type":"firmware_version","major":2,"minor":4,"patch":2}]}}"#;

        // If the newline/carriage return pair aren't interpreted by rust, we
        // wind up with bytes for the literal char sequence slash-n-slash-r,
        // instead of for a newline and a return.
        let json_string = format!("{}\n\r", json_body);

        let mut buf = json_string.as_bytes().to_vec();

        // During test, we do some slightly clumsy things with buffers. This
        // message isn't _actually_ removed from the buffer after read, so we'll
        // need to skip past it before we try to parse results.
        let json_len = buf.len();

        // Artificial Scope forces resources to drop after the scope closes.
        {
            let mut cursor = Cursor::new(&mut buf);
            let reference = true;

            let mut handle = make_api_handler(&mut cursor, reference);

            handle();
        }

        let mut reader = Cursor::new(&buf);
        reader.set_position(json_len.try_into().unwrap());

        let mut got = String::new();
        reader
            .read_to_string(&mut got)
            .expect("Failed to read back a string");

        assert_equal!(expeced_response, got);
    }
}
